<!DOCTYPE html>
<html>
    <head>
        <title>Profile</title>
        <link rel="stylesheet" href="styles/common.css">
        <link rel="stylesheet" href="pages-css/profile.css">
        <link rel="shortcut icon" href="images/icon.png" type="image/x-icon">
    </head>
    <body>
        <header>
            <div class="menu">
                <div class = "title">Sempai</div>
            </div>

            <div class="menu"></div>
                <img src="images/logo.gif" alt="" style="height: 50px;">
            </div>
        </header>
        <?php
        header('content-type: text/html; charset=utf-8');
        if (!((isset($_COOKIE["islogged"]))and($_COOKIE["islogged"]))){
            echo "<script> location.href='http://sempai/index.php'; </script>";
        }
        $login = $_COOKIE['login'];
        if(($handle = fopen("users.csv","r")) !== FALSE){
            while(($data = fgetcsv($handle,1000,";")) !== FALSE ){
                for($c=0; $c<count($data); $c++){
                    if (isset($data[$c*9]) && isset($data[$c*9+1])){
                        if ($login == $data[$c*9]){
                            $surname = $data[$c * 9 + 2];
                            $name = $data[$c * 9 + 3];
                            $patronimyc = $data[$c * 9 + 4];
                            $day = $data[$c * 9 + 5];
                            $month = $data[$c * 9 + 6];
                            $year = $data[$c * 9 + 7];
                            $gender = $data[$c * 9 + 8];
                            break;
                        }
                    }
                }
            }
            fclose($handle);
        }
        ?>
        <div class="bg">
            <?php
            if((file_exists('interests.txt'))and($file = fopen('interests.txt', 'r')) !== FALSE){
                $data = unserialize(fgets($file));
                $intetests = $data[6];
                if ($data[0])
                    $drama = "Drama";
                if ($data[1])
                    $romantic = "Romantica";
                if ($data[2])
                    $isekai = "Isekai";
                if ($data[3])
                    $comedy = "Comedy";
                if ($data[4])
                    $shounen = "Shounen";
                if ($data[5])
                    $shoujo = "Shoujo";
                fclose($file);
            }
            if((file_exists('marks.txt'))and($file = fopen('marks.txt', 'r')) !== FALSE){
                $data = unserialize(fgets($file));
                if(isset($data)){
                    $comonbt = $data[0] + $data[1] + $data[2];
                    $middlebt = $comonbt/3;
                    $comonbg = $data[3] + $data[4] + $data[5];
                    $middlebg = $comonbg/3;
                }
                fclose($file);
                arsort($data);
                $text = "";
                foreach ($data as $key => $val){
                    $text .= $val."; ";
                }
            }
            ?>
            <div class="row">
                <div class="main-info">
                    <img class="profile-image" src="images/default-profile-image.png" alt="" style="height: 200px;">
                </div>
                <div class="main-info">
                    <div class="names">
                        <p><?php echo $login; ?></p>
                        <p><?php echo $surname; ?> <?php echo $name; ?> <?php echo $patronimyc; ?></p>
                        <p><?php echo $day; ?> <?php echo $month; ?> <?php echo $year; ?> <?php echo $gender; ?></p>
                        <button class="details-button">
                            details
                        </button>
                    </div>
                </div>
                <div class="main-info">
                    <form action="#" method="post">
                        <button type="submit" name="unlog">
                            Unlog
                        </button>
                    </form>
                    <?php
                    if (isset($_REQUEST["unlog"])) {
                        setcookie("islogged", false);
                        $new_url = 'https://sempai/index.php';
                        header('Location: '.$new_url);
                    }
                    ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="details-fields">
                    <h1>Hobbies</h1>
                    <p><?php echo $intetests; ?></p>
                </div>
                <div class="details-fields">
                    <h1>Favourite genres</h1>
                    <p><?php echo $drama; ?></p>
                    <p><?php echo $romantic; ?></p>
                    <p><?php echo $comedy; ?></p>
                    <p><?php echo $isekai; ?></p>
                    <p><?php echo $shounen; ?></p>
                    <p><?php echo $shoujo; ?></p>
                </div>
                <div class="details-fields">
                    <h1>School marks</h1>
                    <p>Common ball tochnye = <?php echo $comonbt; ?></p>
                    <p>Middle ball tochnye = <?php echo $middlebt; ?></p>
                    <p>Common ball gym = <?php echo $comonbg; ?></p>
                    <p>Middle ball gym = <?php echo $middlebg; ?></p>
                    <p><?php echo $text; ?></p>
                </div>
                <button class="details-edit">
                    Edit
                </button>
            </div>
            <hr>
            <div class="row">
                <button class="buts" name="interes" onclick="window.location.href = 'details.php';">
                    interests
                </button>
                <button class="buts" name="school" onclick="window.location.href = 'detailsSch.php';">
                    school info
                </button>
            </div>
        </div>
    </body>
</html>