<!DOCTYPE html>
<html>
    <head>
        <title>Sempai</title>
        <link rel="stylesheet" href="pages-css/authorization.css">
        <link rel="stylesheet" href="styles/common.css">
        <link rel="shortcut icon" href="images/icon.png" type="image/x-icon">
    </head>
    <body>
        <header>
            <div class="menu">
                <div class = "title">Sempai</div>
            </div>

            <div class="menu"></div>
                <img src="images/logo.gif" alt="" style="height: 50px;">
            </div>
        </header>
        <div class="auth">
            <form action="#" method="post">
                <div class="row1">
                    <input class="auth-bars" type="login" name="login" placeholder="login" required/>
                    <input class="auth-bars" type="password" name="password" placeholder="password" required/>
                    <div class="auth-bars">
                        <button class="sign-in" type="submit" name="enter">
                            sign in
                        </button>
                    </div>
                </div>
                <div class="row2">
                    <div class="sign-up-bar">
                        <p class="p">
                            Forgot your password?
                        </p>
                    </div>
                    
                    <div class="sign-up-bar">
                        <button class="recovery">
                            recovery
                        </button>
                    </div>
                    
                    <div class="sign-up-bar">
                        <p class="p">
                            Don't have an account?
                        </p>
                    </div>
                    
                    <div class="sign-up-bar">
                        <button class="sign-up" type="button" onclick="window.location.href = 'register.php';">
                            sign up
                        </button>
                    </div>
                </div>
            </form>
            <?php
            session_start();
            define('DOBAVKA1','931a30b234c');
            define('DOBAVKA2','f32291c0cc0ad0');
            if ((isset($_COOKIE["islogged"]))and($_COOKIE["islogged"])){
                echo "<script> location.href='http://sempai/profile.php'; </script>";
            }
            if (isset($_REQUEST["enter"])){
                $forms="login;password;surname;name;patronymic;day;month;year;gender";
                $forms_array=explode(";",$forms);
                $user=[];
                
                $user[0]=iconv('utf-8//IGNORE', 'windows-1251//IGNORE',$_POST['login']);
                $user[1]=iconv('utf-8//IGNORE', 'windows-1251//IGNORE', md5(DOBAVKA1.$_POST['password'].DOBAVKA2));
                $flag=false;
                if(($handle = fopen("users.csv","r")) !== FALSE){
                    while(($data = fgetcsv($handle,1000,";")) !== FALSE ){
                        //$num = count($data);// кол-во полей в строке
                        for($c=0; $c<count($data); $c++){
                            if (isset($data[$c*9]) && isset($data[$c*9+1])){
                                if ($user[0] == $data[$c*9] && $user[1]==$data[$c*9+1]){ 
                                    $flag=true; $index=$c; $_SESSION['user']=$_POST['login']; 
                                    break;
                                }
                            }
                        }
                    }
                    fclose($handle);
                }
                if($flag){
                    setcookie("islogged", true);
                    setcookie("login", $user[0]);
                    echo "<script> location.href='http://sempai/profile.php'; </script>";
                }
                else echo "Пароль введен неверно";
            }
            ?>
        </div>
    </body>
</html>