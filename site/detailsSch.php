<!DOCTYPE html>
<html>
    <head>
        <title>Details</title>
        <link rel="stylesheet" href="styles/common.css">
        <link rel="stylesheet" href="pages-css/details.css">
        <link rel="shortcut icon" href="images/icon.png" type="image/x-icon">
    </head>
    <body>
        <header>
            <div class="menu">
                <div class = "title">Sempai</div>
            </div>

            <div class="menu"></div>
                <img src="images/logo.gif" alt="" style="height: 50px;">
            </div>
        </header>
        <div class="bg">
            <form action="#" method="post">
                <div class="intereses">
                    <p class="marks">Your marks</p>
                    <div class="marks">
                        <label for="math">Math</label><br>
                        <input type="radio" name="math" value="0"required/>
                        <label for="0">0</label>
                        <input type="radio" name="math" value="1"required/>
                        <label for="0">1</label>
                        <input type="radio" name="math" value="2"required/>
                        <label for="0">2</label>
                        <input type="radio" name="math" value="3"required/>
                        <label for="0">3</label>
                        <input type="radio" name="math" value="4"required/>
                        <label for="0">4</label>
                        <input type="radio" name="math" value="5"required/>
                        <label for="0">5</label>
                    </div>
                    <div class="marks">
                    <label for="math">Shizika</label><br>
                        <input type="radio" name="shiz" value="0"required/>
                        <label for="0">0</label>
                        <input type="radio" name="shiz" value="1"required/>
                        <label for="0">1</label>
                        <input type="radio" name="shiz" value="2"required/>
                        <label for="0">2</label>
                        <input type="radio" name="shiz" value="3"required/>
                        <label for="0">3</label>
                        <input type="radio" name="shiz" value="4"required/>
                        <label for="0">4</label>
                        <input type="radio" name="shiz" value="5"required/>
                        <label for="0">5</label>
                    </div>
                    
                    <div class="marks">
                    <label for="math">Himiya</label><br>
                        <input type="radio" name="him" value="0"required/>
                        <label for="0">0</label>
                        <input type="radio" name="him" value="1"required/>
                        <label for="0">1</label>
                        <input type="radio" name="him" value="2"required/>
                        <label for="0">2</label>
                        <input type="radio" name="him" value="3"required/>
                        <label for="0">3</label>
                        <input type="radio" name="him" value="4"required/>
                        <label for="0">4</label>
                        <input type="radio" name="him" value="5"required/>
                        <label for="0">5</label>
                    </div>
                    
                    <div class="marks">
                    <label for="math">History</label><br>
                        <input type="radio" name="hist" value="0"required/>
                        <label for="0">0</label>
                        <input type="radio" name="hist" value="1"required/>
                        <label for="0">1</label>
                        <input type="radio" name="hist" value="2"required/>
                        <label for="0">2</label>
                        <input type="radio" name="hist" value="3"required/>
                        <label for="0">3</label>
                        <input type="radio" name="hist" value="4"required/>
                        <label for="0">4</label>
                        <input type="radio" name="hist" value="5"required/>
                        <label for="0">5</label>
                    </div>
                    
                    <div class="marks">
                    <label for="math">Literature</label><br>
                        <input type="radio" name="lit" value="0"required/>
                        <label for="0">0</label>
                        <input type="radio" name="lit" value="1"required/>
                        <label for="0">1</label>
                        <input type="radio" name="lit" value="2"required/>
                        <label for="0">2</label>
                        <input type="radio" name="lit" value="3"required/>
                        <label for="0">3</label>
                        <input type="radio" name="lit" value="4"required/>
                        <label for="0">4</label>
                        <input type="radio" name="lit" value="5"required/>
                        <label for="0">5</label>
                    </div>
                    
                    <div class="marks">
                    <label for="math">Painting</label><br>
                        <input type="radio" name="pai" value="0"required/>
                        <label for="0">0</label>
                        <input type="radio" name="pai" value="1"required/>
                        <label for="0">1</label>
                        <input type="radio" name="pai" value="2"required/>
                        <label for="0">2</label>
                        <input type="radio" name="pai" value="3"required/>
                        <label for="0">3</label>
                        <input type="radio" name="pai" value="4"required/>
                        <label for="0">4</label>
                        <input type="radio" name="pai" value="5"required/>
                        <label for="0">5</label>
                    </div>
                </div>
            </div>
            
            <div class="bottom">
                <button class="save" type="submit" name="save">
                    Save
                </button>
            </form>
            <?php

            if (isset($_REQUEST["save"])) {
                $forms = "math;shiz;him;hist;lit;pai";
                $forms_array = explode(";", $forms);

                $user = [];
                $pred = [];
                for ($i=0; $i <6 ; $i++) {
                    $user[$i] = $_POST[$forms_array[$i]];
                }

                $file = fopen('marks.txt', 'a+');
                $user= serialize($user);
                fputs($file,$user);
                fclose($file);
                echo "<script> location.href='http://sempai/profile.php'; </script>";
            }

            ?>
        </div>
        
    </body>
</html>