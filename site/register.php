<!DOCTYPE html>
<html>
    <head>
        <title>Registration</title>
        <link rel="stylesheet" href="styles/common.css">
        <link rel="stylesheet" href="pages-css/registration.css">
        <link rel="shortcut icon" href="images/icon.png" type="image/x-icon">
    </head>
    <body>
        <header>
            <div class="menu">
                <div class = "title">Sempai</div>
            </div>

            <div class="menu"></div>
                <img src="images/logo.gif" alt="" style="height: 50px;">
            </div>
        </header>
        <div class="bg">
            <form action="#" method="post">
                <div class="row">
                    <p class="p1">Form</p>
                </div>

                <div class="row">
                    <input type="login" class="log" name="login" placeholder="login" pattern="[a-zA-Z0-9]+" required/>
                    <input type="password" class="log" name="password" placeholder="password" required/>
                </div>
                
                <div class="row">
                    <input type= "surname" class="fio" name="surname" placeholder="surname" required/>
                    <input type= "name" class="fio" name="name" placeholder="name" required/>
                    <input type= "patronymic" class="fio" name="patronymic" placeholder="patronymic">
                </div>
                
                <div class="row">
                    <label for="date-of-birth" class="date-lable">Date of birth</label>
                        <div>
                            <select tabindex="3" name="day" class="date-of-birth">
                                <option selected disabled value="">День</option>
                                <?php
                                    for ($i=1; $i <32; $i++) {
                                    echo "<option value=\"$i\">$i</option>";
                                    }
                                ?>
                            </select>
                            <select tabindex="4" name="month" class="date-of-birth">
                                <option selected disabled value="">Месяц</option>
                                <?php
                                    $months=['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
                                    for ($i=0; $i <12; $i++) {
                                    echo "<option value=\"$months[$i]\">$months[$i]</option>";
                                    }
                                ?>
                            </select>
                            <select tabindex="5" name="year" class="date-of-birth">
                                <option selected disabled value="">Год</option>
                                <?php
                                    for ($i=1950; $i <2022; $i++) {
                                    echo "<option value=\"$i\">$i</option>}";
                                    }
                                ?>
                            </select>
                            <fieldset class="date-of-birth">
                                <div class="gender">
                                    <input type="radio" name="gender" value="Male"required/>
                                    <label for="male">Male</label>
                                </div>
                                <div class="gender">
                                    <input type="radio" name="gender" value="Female"required/>
                                    <label for="female">Female</label>
                                </div>
                            </fieldset>
                    </div>
                </div>
                
                <div class="row">
                    <label for="signs" class="p2">Already have an account?</label>
                    <button class="signs" type="button" onclick="window.location.href = 'index.php';">
                        sign in
                    </button>
                    <input type="checkbox" name="agreement" id="agreement" required/>
                    <label for="agreement" class="p2">Do you accept the user agreement?</label>
                    <button class="signs" name="reg" type="submit">
                        sign up
                    </button>
                </div>
            </form>
            <?php
                define('DOBAVKA1','931a30b234c');
                define('DOBAVKA2','f32291c0cc0ad0');
                if (isset($_REQUEST["reg"])){
                    $forms="login;password;surname;name;patronymic;day;month;year;gender";
                    $forms_array=explode(";",$forms);
                    $password = md5(DOBAVKA1.$_POST['password'].DOBAVKA2);
                    
                    $user=[];
                    for ($i=0; $i <9 ; $i++) {
                        $user[$i]=$_POST[$forms_array[$i]];
                    }
                    $user[1]=$password;
                    
                    $flag=false;
                    $index=0;
                    $logins=[];
                    if(($handle = fopen('users.csv', 'r')) !== FALSE){
                        while(($data = fgetcsv($handle,1000,";")) !== FALSE ){
                            for ($i=0; $i < count($data); $i++) {
                                if(isset($data[$i*9])){
                                    $logins[$index]=$data[$i*9];
                                    $index++;
                                }
                            }
                        }
                        fclose($handle);
                    }
                
                    for ($i=0; $i < $index; $i++) {
                        if($user[0]==$logins[$i]) { 
                            $flag = true; 
                            echo "Пользователь с таким логином уже существует. ";
                        }
                    }
                
                    if ($user[5] == 'Февраль' && (($user[4]>=29 && $user[6]%4!=0) || ($user[4]>29 && $user[6]%4==0))) {
                        echo "Ошибка в определении даты рождения";
                        $flag=true;
                    }
                
                    //запись данных в файл
                    if (!$flag) {
                        $file = fopen('users.csv', 'a+');
                        for ($i=0; $i <9 ; $i++) {
                            $user[$i]= iconv('utf-8//IGNORE', 'windows-1251//IGNORE', $user[$i]);
                        }
                        fputcsv($file, $user,';');
                        fclose($file);
                        echo "<script> location.href='http://sempai/index.php'; </script>";
                    }
                    else echo 'Заполните все поля!';
                }
            ?>
        </div>
    </body>
</html>