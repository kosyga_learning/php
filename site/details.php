<!DOCTYPE html>
<html>
    <head>
        <title>Details</title>
        <link rel="stylesheet" href="styles/common.css">
        <link rel="stylesheet" href="pages-css/details.css">
        <link rel="shortcut icon" href="images/icon.png" type="image/x-icon">
    </head>
    <body>
        <header>
            <div class="menu">
                <div class = "title">Sempai</div>
            </div>

            <div class="menu"></div>
                <img src="images/logo.gif" alt="" style="height: 50px;">
            </div>
        </header>
        <div class="bg">
            <form action="#" method="post">
                <div class="intereses">
                    <p class="genres">Favorite anime genres</p>
                    <div class="genres">
                        <input type="checkbox" name="drama" id="" value="True">
                        <label for="drama">Drama</label><br>
                    </div>
                    <div class="genres">
                        <input type="checkbox" name="romantic" id="" value="True">
                        <label for="romantic">Romantica</label><br>
                    </div>
                    
                    <div class="genres">
                        <input type="checkbox" name="comedy" id="" value="True">
                        <label for="comedy">Comedy</label><br>
                    </div>
                    
                    <div class="genres">
                        <input type="checkbox" name="shonen" id="" value="True">
                        <label for="shonen">Shonen</label><br>
                    </div>
                    
                    <div class="genres">
                        <input type="checkbox" name="shoujo" id="" value="True">
                        <label for="shoujo">Shoujo</label><br>
                    </div>
                    
                    <div class="genres">
                        <input type="checkbox" name="isekai" id="" value="True">
                        <label for="isekai">Isekai</label><br>
                    </div>
                </div>
                
                <div class="intereses">
                    <p class="genres">Hobbies</p>
                    <input type="text" class="hobby" name="hobby">
                </div>
            </div>
            
            <div class="bottom">
                <button class="save" type="submit" name="save">
                    Save
                </button>
            </form>
            <?php

            if (isset($_REQUEST["save"])) {
                $forms = "drama;romantic;isekai;comedy;shounen;shoujo;hobby";
                $forms_array = explode(";", $forms);

                $user=[];
                for ($i=0; $i <7 ; $i++) {
                    $user[$i]=$_POST[$forms_array[$i]];
                }

                $file = fopen('interests.txt', 'a+');
                $user= serialize($user);
                fputs($file,$user);
                fclose($file);
                echo "<script> location.href='http://sempai/profile.php'; </script>";
            }

            ?>
        </div>
        
    </body>
</html>